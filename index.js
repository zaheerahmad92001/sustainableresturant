import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import {Provider}from 'react-redux';
import { store } from './Src/Redux/store';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import React, { useState, useEffect, useRef } from 'react'


const RNRedux = (props) => {
    
    return (
        <SafeAreaProvider>
        <Provider store={store}>
            {/* <PersistGate loading={null} persistor={persistor}> */}
                <App/>
            {/* </PersistGate> */}
        </Provider>
        </SafeAreaProvider>
    )
}

AppRegistry.registerComponent(appName, () => RNRedux);
