import react from 'react';
import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { appMarginTop } from '../../Constants/appStyle';
import {black, White} from '../../Constants/colors';
import {mediumText, smallText} from '../../Constants/fonts';

const styles = StyleSheet.create({
  
  wraper:{
    flex:1,
    backgroundColor:White,
  },

  header: {
    flexDirection: 'row',
    marginTop:appMarginTop,
    alignItems:'center',
    backgroundColor:White,
  },
  textA: {
    color: black,
    marginLeft: wp(42.5),
    fontSize:mediumText,
    fontWeight:'500'
  },

  container: {
    backgroundColor: White,
    flex:1,
    marginTop:20,
  },
  text: {
    width: wp(60),
    fontSize: smallText,
    fontWeight: '700',
    color: black,
    textAlign: 'center',
  },
});

export default styles;
