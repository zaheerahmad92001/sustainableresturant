import React, {Component} from 'react';
import {Text, View ,FlatList} from 'react-native';
import {black, DANGER, TEXT_INPUT_LABEL, White} from '../../Constants/colors';
import {mediumText, smallText} from '../../Constants/fonts';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import styles from './style';
import Restaurant from '../../Components/Resturants';

const data=[
  {id:1},
  {id:1},
  {id:1},
  {id:1},
  {id:1},
]

export default class Favourites extends Component {
  constructor(props){
    super(props)
    this.state={
      
    }
  }

  renderFavResturants =({item , index})=>{
   return(
     <Restaurant/>
   )
  }

  render() {
    return (
      <View style={styles.wraper}>
        <View style={styles.header}>
          <Text style={styles.textA}>Favourites</Text>
        </View>
        <View style={styles.container}>
          
        <FlatList
        data={data}
        renderItem={this.renderFavResturants}
        keyExtractor={(item)=>item.id}
        />

        </View>
      </View>
    );
  }
}
