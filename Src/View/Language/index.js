import React, { Component } from "react";
import { View, Text, TouchableOpacity } from 'react-native'
import { Divider, Icon } from "react-native-elements";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { lightGrey } from "../../Constants/colors";
import styles from "./styles";

class Language extends Component {

    constructor(props) {
        super(props)
        this.state = {
           it_selected:false,
           en_selected:true,
        }
    }

handleLanguage=(text)=>{
    if(text=='italian'){
        this.setState({it_selected:true , en_selected:false})
    }else{
        this.setState({it_selected:false , en_selected:true})
    }
}

    render() {
        const {it_selected , en_selected} = this.state
        return (
            <View style={styles.wraper}>
                <View style={styles.header}>
                    <Icon
                        onPress={() => this.props.navigation.pop()}
                        name='ios-arrow-back-sharp'
                        type='ionicon'
                        iconStyle={styles.iconStyle}
                    />
                    <Text style={styles.mediumText}>Language</Text>
                    <TouchableOpacity>
                        <Text style={styles.mediumText}>Save</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.content}>
                    <TouchableOpacity 
                    onPress={()=>this.handleLanguage('italian')}
                    style={styles.langStyle}>
                        <View style={styles.rowSp}>
                            <Text style={styles.mediumText}>Italian</Text>
                            {it_selected ?
                            <Icon
                                name="check"
                                type="feather"
                                iconStyle={styles.checkStyle}
                            />: null }
                        </View>
                    </TouchableOpacity>
                    <Divider style={styles.divider}/>



                    <TouchableOpacity 
                    onPress={()=>this.handleLanguage('english')}
                    style={{marginTop:20}}>
                        <View style={styles.rowSp}>
                            <Text style={styles.mediumText}>English</Text>
                            {en_selected ?
                            <Icon
                                name="check"
                                type="feather"
                                iconStyle={styles.checkStyle}
                            />: 
                            null
                            }
                        </View>
                    </TouchableOpacity>
                    <Divider style={styles.divider}/>
                </View>


            </View>
        )
    }
}

export default Language