import React, { useEffect } from "react";
import { View, Text, Image } from 'react-native'
import { foe, fos } from '../../Constants/images'
import { CommonActions } from '@react-navigation/native';
import { StackActions } from '@react-navigation/native';


// import { NavigationActions } from 'react-navigation';

// const navigateAction = NavigationActions.navigate({
//   routeName: 'Profile',

//   params: {},

//   action: NavigationActions.navigate({ routeName: 'SubProfileRoute' }),
// });

// this.props.navigation.dispatch(navigateAction);

import styles from "./styles";

function SplashScreen({ navigation }) {
    useEffect(() => {
        setTimeout(() => {
            navigation.dispatch(StackActions.replace('BottomTab', {
                screen: 'Home',
            }))
        }, 2000);
    }, [])

    return (
        <View style={styles.wraper}>
            <View style={styles.content}>
                <View style={styles.row}>
                    <Image
                        source={foe}
                        style={styles.imgStyle}
                    />
                    <Image
                        source={fos}
                        style={styles.imgStyle}
                    />
                </View>
            </View>
        </View>
    )
}
export default SplashScreen

