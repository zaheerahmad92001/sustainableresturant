import React from "react";
import { StyleSheet } from "react-native";
import { DANGER } from "../../Constants/colors";

const styles = StyleSheet.create({
 wraper:{
     flex:1,
     backgroundColor:DANGER
 },
 content:{
     flex:1,
     justifyContent:'center',
     alignItems:'center'
 },
 row:{
     flexDirection:'row',
     alignContent:'center',
     justifyContent:'center',
 },
 imgStyle:{
     width:150,
     height:200,
     marginHorizontal:10,
 }
})
export default styles