import react from 'react';
import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { appMarginTop } from '../../Constants/appStyle';
import {black, grey, lightGrey, offwhite, TEXT_INPUT_LABEL, White} from '../../Constants/colors';
import {mediumText, smallText} from '../../Constants/fonts';

const styles = StyleSheet.create({
  wraper:{
    flex:1,
    backgroundColor:White
  },
  header:{
    flexDirection: 'row',
    paddingTop:appMarginTop,
    alignItems:'center',
    backgroundColor:offwhite,
    paddingBottom:10,

  },
  textA: {
    color: black,
    marginLeft: wp(42.5),
    fontSize:mediumText,
    fontWeight:'500'
  },
  row:{
    flexDirection:'row',
    alignItems:'center',
  justifyContent:'space-between',
  },
  innRow:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
  },
  iconStyle:{
    fontSize:15,
    marginLeft:10,
    color:grey
  },
  standeredText:{
    fontSize:mediumText,
    color:black,
    fontWeight:'500',
  },
  smallText:{
    fontSize:12,
    color:grey,
    fontWeight:'500'
  },
  TopSpace:{
    marginTop:20
  },





  btnS: {
    width: wp(100),
    height: hp(7.5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnST: {
    color: black,
    textAlign: 'center',
    fontSize: mediumText,
    fontWeight: '700',
    marginTop: hp(2),
  },
  btnL: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: White,
    alignItems: 'center',
  },
  btnT: {
    color: black,
    fontWeight: '600',
    fontSize: 16,
  },
  btnE: {
    color: TEXT_INPUT_LABEL,
    fontWeight: '600',
    fontSize: 16,
    // marginRight: wp(2),
  },
  btn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: White,
    height: hp(6),
    alignItems: 'center',
    width: wp(100),
    marginTop: 2,
  },
  btnText: {
    color: black,
    fontWeight: '600',
    fontSize: 16,
    marginLeft: wp(2),
  },
  btnSideText: {
    color: TEXT_INPUT_LABEL,
    fontWeight: '600',
    fontSize: 16,
    marginRight: wp(2),
  },
  btnArrow: {
    width: 15,
    height: 15,
    tintColor: TEXT_INPUT_LABEL,
    marginRight: wp(2),
  },
});

export default styles;
