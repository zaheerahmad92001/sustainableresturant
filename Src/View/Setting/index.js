import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Switch,
  Linking,
  Platform
} from 'react-native';
import React, { Component } from 'react';

import { grey, White } from '../../Constants/colors';
import styles from './style';
import { Divider, Icon } from 'react-native-elements';
import Share from 'react-native-share';

export default class Setting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isEnabled: false,
    };
  }

  toggleSwitch = () => {
    const { isEnabled } = this.state
    this.setState({
      isEnabled: !isEnabled
    })
  }

  sendEmail = () => {
    let email = 'info@friendofthesea.org'
    let mail = `mailto:${email}?subject=friendofthesea&body=Description`

    Linking.canOpenURL(mail)
      .then(supported => {
        if (!supported) {
          Alert.alert('unable to opem email');
        } else {
          console.log('yes can open')
          return Linking.openURL(mail);
        }
      }).catch(err => console.log(err));
  }

  shareApp = () => {
    const options = {
      title: 'React native Mobile App developer',
      message: 'Mobile development',
      url: 'URL which you want to share',
      //social: Share.Social.WHATSAPP,
      social: Share.Social,
      whatsAppNumber: "999999999",  // country code + phone number
      filename: 'test', // only for base64 file in Android 
    };
    Share.open(options)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        err && console.log(err);
      });
  }


  openStore = () => {
    const GOOGLE_PACKAGE_NAME = 'agrawal.trial.yourfeedback';
    // app.petmypal
    const APPLE_STORE_ID = 'id284882215';
    //This is the main trick
    if (Platform.OS != 'ios') {
      Linking.canOpenURL(`market://details?id=${GOOGLE_PACKAGE_NAME}`)
        .then((supported) => {
          if (!supported) {
            console.log('unable to open Google play console')
          } else {
            console.log('yes can open');
            Linking.openURL(`market://details?id=${GOOGLE_PACKAGE_NAME}`)
          }
        }).catch((error) => {
          console.log('error while opening Google play consoe', error)
        })

    } else {
      Linking.canOpenURL(`itms://itunes.apple.com/in/app/apple-store/${APPLE_STORE_ID}`)
        .then((supported) => {
          if (!supported) {
            console.log('unable to open itume',)
          } else {
            Linking.openURL(`itms://itunes.apple.com/in/app/apple-store/${APPLE_STORE_ID}`,)

          }
        }).catch((error) => {
          console.log('error while opening', error)
        })

    }
  };

 openPrivacy=()=>{
  let url = 'https://friendofthesea.org/privacy-policy/'
  Linking.canOpenURL(url)
        .then((supported) => {
          if (supported) {
              console.log('supported' , supported)
            return Linking.openURL(url);
          } else {
            console.log('something went wrong')
          }
        })
        .catch(() => {
          if (Platform.OS === 'ios') {
            Linking.openURL(
              `maps://?q=${latitude},${longitude}`,
            );
          }
        });
      }

  render() {
    const { isEnabled } = this.state;
    return (
      <View style={styles.wraper}>
        <View style={styles.header}>
          <Text style={styles.textA}>Settings</Text>
        </View>

        <ScrollView 
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 20, marginHorizontal: 15 }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Language')}
          >
            <View style={styles.row}>
              <Text style={styles.standeredText}>Language</Text>
              <View style={styles.innRow}>
                <Text style={[styles.smallText,{fontSize:14}]}>English</Text>
                <Icon name='arrow-right' type='simple-line-icon'
                  iconStyle={styles.iconStyle}
                />
              </View>
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:15,}}/>


          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('MetricSystem')}
            style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>Metric system</Text>
              <View style={styles.innRow}>
                <Text style={[styles.smallText,{fontSize:14}]}>Km</Text>
                <Icon name='arrow-right' type='simple-line-icon'
                  iconStyle={styles.iconStyle}
                />
              </View>
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:15,}}/>


          <View style={{marginTop:15}}>
            <View style={{
              flexDirection:'row',
              alignItems:'center',
              justifyContent:'space-between',
            }}>
              <Text style={styles.standeredText}>Push Notification</Text>
              <Switch
              trackColor={{ false: 'grey', true: '#99C68E' }}
              thumbColor={isEnabled ? 'green' : White}
              ios_backgroundColor={isEnabled ? "green" : grey}
              onValueChange={() => this.toggleSwitch()}
              value={isEnabled}
            />
            </View>
            <Text style={styles.smallText}>Tell us about restaurants close to you</Text>

            
          </View>


          <Divider style={{marginTop:15,}}/>




          <View style={styles.TopSpace}>
            <Text style={styles.smallText}>Friend Of The Sea contacts</Text>
          </View>


          <TouchableOpacity
            onPress={() => this.sendEmail()}
            style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>info@friendofthesea.org</Text>
              <Icon name='arrow-right' type='simple-line-icon'
                iconStyle={styles.iconStyle}
              />
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:10,}}/>


          <TouchableOpacity style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>www.friendofthesea.org</Text>
              <Icon name='arrow-right' type='simple-line-icon'
                iconStyle={styles.iconStyle}
              />
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:10,}}/>



          <View style={styles.TopSpace}>
            <Text style={styles.smallText}>Friend Of The Earth contacts</Text>
          </View>


          <TouchableOpacity
            onPress={() => this.sendEmail()}
            style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>info@friendofthesea.org</Text>
              <Icon name='arrow-right' type='simple-line-icon'
                iconStyle={styles.iconStyle}
              />
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:10,}}/>



          <TouchableOpacity style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>www.friendofthesea.org</Text>
              <Icon name='arrow-right' type='simple-line-icon'
                iconStyle={styles.iconStyle}
              />
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:10,}}/>



          <TouchableOpacity
            onPress={() => this.shareApp()}
            style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>Share the app</Text>
              <Icon name='arrow-right' type='simple-line-icon'
                iconStyle={styles.iconStyle}
              />
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:10,}}/>


          <TouchableOpacity
            onPress={() => this.openStore()}
            style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>Rate the app</Text>
              <Icon name='arrow-right' type='simple-line-icon'
                iconStyle={styles.iconStyle}
              />
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:10,}}/>





          <TouchableOpacity 
          onPress={()=>this.openPrivacy() }
          style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>Privacy Policy</Text>
              <Icon name='arrow-right' type='simple-line-icon'
                iconStyle={styles.iconStyle}
              />
            </View>
          </TouchableOpacity>
          <Divider style={{marginTop:10,}}/>



          {/* <View style={styles.TopSpace}>
            <Text style={styles.smallText}> Technical Support</Text>
          </View> */}

          {/* <TouchableOpacity style={styles.TopSpace}>
            <View style={styles.row}>
              <Text style={styles.standeredText}>PCDUE Support IT</Text>
              <Icon name='arrow-right' type='simple-line-icon'
                iconStyle={styles.iconStyle}
              />
            </View>
          </TouchableOpacity> */}
          {/* <Divider style={{marginTop:10,}}/> */}

        </ScrollView>
      </View>
    );
  }
}
