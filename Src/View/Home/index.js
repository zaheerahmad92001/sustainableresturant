import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Animated,
  Pressable,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';


import { foe, fos,foe_on, foe_off, fos_on, fos_off, search, location, map, } from '../../Constants/images'
import { black, DANGER, TEXT_INPUT_LABEL, White, yellow } from '../../Constants/colors';
import styles from './styles';
import Restaurant from '../../Components/Resturants';
import MyMaps from '../../Components/MyMaps';
import RNGooglePlaces from 'react-native-google-places';
import { heightPercentageToDP, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { appMarginTop } from '../../Constants/appStyle';
import {ChangeLanguage} from '../../Redux/actions/LanguageAction'
import I18n from '../../i18n';

const data = [
  { id: 1 },
  { id: 2 },
  { id: 3 },
  { id: 4 },
  { id: 5 },
  { id: 6 },
]

 class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {
      searchValue: '',
      all_active: true,
      fos_active: false,
      foe_active: false,
      fav_active: false,
      map_Active: false,
      animationValue: new Animated.Value(14),
      // animationValue: new Animated.Value(wp(80)),
      // animationValue : new Animated.Value(wp(80)),
      viewState: true
    }
  }

  AllTabPress = () => {
    this.setState({
      all_active: true,
      foe_active: false,
      fos_active: false,
      fav_active: false,
    })
  }

  handleFOE = () => {
    this.setState({
      all_active: false,
      foe_active: true,
      fos_active: false,
      fav_active: false,
    })
  }

  handleFOS = () => {
    this.setState({
      all_active: false,
      foe_active: false,
      fos_active: true,
      fav_active: false,
    })
  }
  handleFav = () => {
    this.setState({
      all_active: false,
      foe_active: false,
      fos_active: false,
      fav_active: true,
    })
  }

  renderResturants = ({ item, index }) => {
    return (
      <Restaurant 
      item={item}
      onPress={()=>this.handleDetail()}
      />
    )
  }

  handleDetail=()=>{
    this.props.navigation.navigate('Detail')
  }

  switch_Map_List = () => {
    const { map_Active } = this.state
    this.setState({ map_Active: !map_Active })
  }

  openSearchModal() {
    RNGooglePlaces.openAutocompleteModal()
      .then((place) => {
        console.log(place);
      })
      .catch(error => console.log(error.message));  // error is a Javascript Error object
  }

  toggleAnimation = () => {
    let language ='en'
    I18n.locale = 'en'
    this.props.changeLanguage(language)

    if (this.state.viewState == true) {
      Animated.timing(this.state.animationValue, {
        toValue:40,
        // toValue: wp(93),
        timing: 1500
      }).start(() => {
        this.setState({ viewState: false })
      });
    }
    else {
      Animated.timing(this.state.animationValue, {
        // toValue: wp(80),
        toValue: 14,
        timing: 2500
      }).start(this.setState({ viewState: true })
      );
    }
  }

  render() {
    const {
      searchValue,
      all_active,
      fav_active,
      foe_active,
      fos_active,
      viewState,
      map_Active,
    } = this.state

    const animatedStyle = {
      marginLeft: this.state.animationValue,
    }
    return (
      <View style={styles.wraper}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: "center",
          marginTop: appMarginTop,
        }}>
          <View style={{
            top:10,
            position:'absolute',
            zIndex:viewState? 1:-1,
            left:10,
            }} >
              {!viewState &&
            <Icon
              name='ios-arrow-back-sharp'
              type='ionicon'
              iconStyle={styles.iconStyle}
            />}
          </View>
          <View style={styles.outerView}>
            <Animated.View style={[styles.inputContainer, animatedStyle]}>
              <Image
                style={styles.icon}
                source={search}
              />
              <TextInput
                placeholder={I18n.t('homeScreen')}
                placeholderTextColor={TEXT_INPUT_LABEL}
                onChangeText={(text) => this.setState({ searchValue: text })}
                value={searchValue}
                onPressIn={() => this.toggleAnimation()}
                style={styles.InputStyle}
              />
            </Animated.View>
          </View>
        </View>


        <View style={styles.inputContainer2}>
          <View style={styles.leftView}>
            <Image
              style={styles.icon2}
              source={location}
            />
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.openSearchModal()}
            >
              <Text style={styles.addressText}>Address</Text>
            </TouchableOpacity>

          </View>

          <TouchableOpacity
            onPress={() => this.switch_Map_List()}
            style={styles.righView}
            >
            <View style={styles.innerView}>
              {map_Active ?
                <Icon
                  name='list-ul'
                  type='font-awesome-5'
                  iconStyle={{ fontSize: 15, color: black }}
                /> :
                <Image
                  style={styles.mapIcon}
                  source={map}
                />
              }
              {map_Active ?
                <Text style={styles.textStyle}>List</Text> :
                <Text style={styles.textStyle}> Map</Text>
              }
            </View>
          </TouchableOpacity>

        <Pressable 
           onPress={()=>this.props.navigation.navigate('Notifications')}
          style={styles.notificationStyle}>
          <Icon
           name='notifications-outline'
           type='ionicon'
           iconStyle = {[styles.iconStyle,{marginLeft:10,color:TEXT_INPUT_LABEL}]}
          />
        </Pressable>
          </View>

          
        {/* <View style={styles.tabsView}>
          <TouchableOpacity
            onPress={() => this.AllTabPress()}
            style={all_active ? [styles.btnStyle, { backgroundColor: DANGER,marginLeft:0}] : [styles.btnStyle,{marginLeft:0}]}>
            <Text style={all_active ? [styles.standerdText, { color: White ,left:-4 }] : [styles.standerdText]}>All</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.handleFOE()}
            style={styles.btnStyle}>
            <Image
              source={foe_active?foe_on: foe_off}
              style={styles.tabsIcon}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.handleFOS()}
            style={styles.btnStyle}>
            <Image
              source={fos_active? fos_on:fos_off}
              style={styles.tabsIcon}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.handleFav()}
            style={styles.btnStyle}>
            <View style={fav_active ? [styles.circle, { backgroundColor: yellow }] : [styles.circle]}>
              <Icon
                name='crown'
                type='font-awesome-5'
                iconStyle={{ fontSize: 15, color: White }}
              />
            </View>
          </TouchableOpacity>
        </View> */}

        <View style={styles.middleText}>
          {map_Active ?
            <MyMaps />
            :
            <FlatList
              data={data}
              renderItem={this.renderResturants}
              keyExtractor={(item) => item.id}
            />
          }
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  language: state.LanguageReducer.language
})

const mapDispatchToProps = (dispatch) => ({
  changeLanguage: item => dispatch(ChangeLanguage(item))
})

export default connect (mapStateToProps ,mapDispatchToProps)(Home)
