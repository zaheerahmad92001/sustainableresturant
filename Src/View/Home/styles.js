import react from 'react';
import { StyleSheet , Platform} from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { heightPercentageToDP as hp,widthPercentageToDP as wp,} from 'react-native-responsive-screen';
import { appMarginTop } from '../../Constants/appStyle';
import { black, DANGER, grey, lightGrey, TEXT_INPUT_LABEL, White } from '../../Constants/colors';
import { smallText } from '../../Constants/fonts';

const styles = StyleSheet.create({
  wraper: {
    flex: 1,
  },
  outerView:{
    flexDirection:'row',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: White,
    // marginTop:appMarginTop,
    marginHorizontal: wp(4),
    marginBottom: hp(2),
    paddingLeft:20,
    borderRadius: 10,
  },
  icon: {
    height: 18,
    width: 18,
    tintColor: TEXT_INPUT_LABEL,
  },
  InputStyle: {
    flex: 1,
    paddingVertical:Platform.OS=='android'? 8 : 12,
    paddingLeft: 10,
  },

  leftView: {
    backgroundColor: White,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical:10,
    paddingLeft:12,
    borderRadius: 10,
    width: wp(52),
  },
  
  righView: {
    width: wp(26),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
    borderRadius: 10,
    // paddingLeft: 20,
    // alignSelf:"flex-start",
    paddingVertical:10,
    backgroundColor:White,
    // justifyContent:'space-between',
  },
  innerView: {
    flexDirection: "row",
    alignItems: 'center',
  },

  inputContainer2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems:'center',
    marginHorizontal: wp(4),
  },
  textStyle: {
    fontSize: smallText,
    fontWeight:'400',
    color: black,
    marginLeft: 7,
  },

  tabsView:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
  },
tabsIcon:{
   height:30,
   width: 30,
},
standerdText: {
  fontSize: smallText,
  fontWeight:'400',
  color: black,
},
circle:{
 width:25,
 height:25,
 borderRadius:25/2,
 backgroundColor:'#D7D7D7',
 alignItems:'center',
 justifyContent:'center',
},



  icon2: {
    height: 20,
    width: 20,
    marginLeft: 5,
    tintColor: TEXT_INPUT_LABEL,
  },
  map: {
    flexDirection: 'row',
    height: hp(5),
    width: wp(30),
    marginLeft: wp(40),
    borderRadius: 10,
    backgroundColor: White,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
  },
  mapIcon: {
    height: 15,
    width: 15,
    tintColor: black,
  },
  middleText: {
    // backgroundColor: White,
    marginTop: hp(2),
    flex:1,
    // height: hp(70),
    // width: wp(100),
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  btnStyle: {
    backgroundColor:White,
    width: wp(21.5),
    height: hp(5),
    marginTop: hp(2),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginLeft: wp(2),
  },
  iconStyle:{
    color:black,
    fontSize:25,
  },
  addressText:{
    marginLeft:10,
    fontSize:14,
    color:TEXT_INPUT_LABEL
  },
  notificationStyle:{
    backgroundColor:White,
    justifyContent:'center',
    alignItems:'center',
    paddingRight:10,
    paddingVertical:5,
    borderRadius:10,
  }

});

export default styles;
