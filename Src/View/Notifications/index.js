import React, { Component } from "react";
import { View, Text, FlatList, Pressable } from 'react-native'
import { Icon } from "react-native-elements";
import NotificationList from "../../Components/Notifications";

import styles from './styles'
export default class Notifications extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    renderItem =()=>{
        return(
            <NotificationList/>
        )
    }


    render() {
        return (
            <View style={styles.wraper}>
                
                <View style={styles.header}>
                    <View style={styles.leftView}>
                        <Icon
                            onPress={()=>this.props.navigation.pop()}
                            name="arrowleft"
                            type="antdesign"
                            iconStyle={styles.iconStyle}
                        />
                    </View>
                    <View style={styles.middleView}>
                        <Text style={styles.textA}>Notification</Text>
                    </View>
                </View>

                <View style={styles.contentWraper}>

                 <FlatList
                 data={[1,2,3,4,5,6]}
                 keyExtractor={(item)=>item.id}
                 renderItem={this.renderItem}
                 />

                </View>



            </View>
        )
    }
}