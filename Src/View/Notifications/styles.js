import React from 'react'
import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { appMarginTop } from '../../Constants/appStyle'
import { black, offwhite } from '../../Constants/colors'
import { mediumText } from '../../Constants/fonts'
const styles  = StyleSheet.create({
    wraper:{
        flex:1,
        backgroundColor:'white'
    },
    header:{
        flexDirection: 'row',
        paddingTop:appMarginTop,
        alignItems:'center',
        backgroundColor:offwhite,
        paddingBottom:10,
    
      },
      iconStyle:{
      color:black,
      fontSize:20,  
      },
      textA: {
        color: black,
        fontSize:mediumText,
        fontWeight:'500',
        alignSelf:'center',
        
      },
      leftView:{
        flex:0.2,
        alignItems:'center',
      },
      middleView:{
        flex:0.6,
        alignItems:'center',
      },
      rightView:{
        flex:0.2,
        alignItems:'center',
      },
      contentWraper:{
        marginTop:20,
        marginHorizontal:20,
        flex:1,
      }
})

export default styles
