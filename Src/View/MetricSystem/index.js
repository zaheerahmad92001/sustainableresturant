import React, { Component } from "react";
import { View, Text, TouchableOpacity } from 'react-native'
import { Divider, Icon } from "react-native-elements";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { lightGrey } from "../../Constants/colors";
import styles from "./styles";

class MetricSystem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            kmSelected: true,
            mileSelected: false,
        }
    }

    handlMetricSystem = (text) => {
        const { kmSelected, mileSelected } = this.state
        if (text == 'km') {
            this.setState({ kmSelected: true, mileSelected: false })
        } else {
            this.setState({ kmSelected: false, mileSelected: true })

        }
    }

    render() {
        const {mileSelected , kmSelected} = this.state
        return (
            <View style={styles.wraper}>
                <View style={styles.header}>
                    <Icon
                        onPress={() => this.props.navigation.pop()}
                        name='ios-arrow-back-sharp'
                        type='ionicon'
                        iconStyle={styles.iconStyle}
                    />
                    <Text style={styles.mediumText}>Metric System</Text>
                    <TouchableOpacity>
                        <Text style={styles.mediumText}>Save</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.content}>
                    <TouchableOpacity
                        onPress={() => this.handlMetricSystem('km')}
                        style={styles.langStyle}>
                        <View style={styles.rowSp}>
                            <Text style={styles.mediumText}>Km</Text>
                            {kmSelected ?
                            <Icon
                                name="check"
                                type="feather"
                                iconStyle={styles.checkStyle}
                            />: null 
                            }
                        </View>
                    </TouchableOpacity>
                    <Divider style={styles.divider} />



                    <TouchableOpacity 
                    onPress={() => this.handlMetricSystem('miles')}
                    style={{ marginTop: 20 }}>
                        <View style={styles.rowSp}>
                            <Text style={styles.mediumText}>Miles</Text>
                            {mileSelected ?
                            <Icon
                                name="check"
                                type="feather"
                                iconStyle={styles.checkStyle}
                            />: null}
                        </View>
                    </TouchableOpacity>
                    <Divider style={styles.divider} />
                </View>


            </View>
        )
    }
}

export default MetricSystem