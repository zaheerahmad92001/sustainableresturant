import React from "react";
import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { appMarginTop } from "../../Constants/appStyle";
import { black, DANGER, lightGrey, offwhite, White } from "../../Constants/colors";
import { mediumText, smallText } from "../../Constants/fonts";

 const styles = StyleSheet.create({
    wraper:{
    flex: 1,
    backgroundColor:White,
    },

    header:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:appMarginTop ,
        backgroundColor:White,
    //    backgroundColor:offwhite,
       paddingBottom:10,
       paddingHorizontal:20,

        
    },
    iconStyle:{
        color:black,
        fontSize:25,
      },
      mediumText:{
          fontSize:mediumText,
          fontWeight:"500",
          color:black,
      },
      checkStyle:{
          fontSize:20,
          color:DANGER,
      },
      rowSp:{
          flexDirection:'row',
          alignItems:'center',
          justifyContent:'space-between',
      },
      content:{
          marginHorizontal:20,
          marginTop:hp(2)
      },
      divider:{
          borderBottomWidth:StyleSheet.hairlineWidth,
          borderBottomColor:lightGrey,
          marginTop:20,
      }
})
export default styles