import React, { Component } from "react";
import { View, Text, FlatList, Pressable , StyleSheet } from 'react-native'
import { Icon } from "react-native-elements";
import LegendList from "../../Components/LegendList";
import { fos_on,foe_on } from "../../Constants/images";
import {fosDescription , foeDescription ,GoldDescription}from '../../Constants/constantValues'
import styles from './styles'

let myLegends = [
    {id:1,name:'Friend of the Sea' , img:fos_on , description:fosDescription},
    {id:1,name:'Friend of the Earth' , img:foe_on , description:foeDescription},
    {id:1,name:'Gold' , icon:'crown' , description:fosDescription}
]

export default class Legend extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    renderItem =({item})=>{
        return(
            <LegendList
            item={item}
            />
        )
    }


    render() {
        return (
            <View style={styles.wraper}>
                
                <View style={styles.header}>
                    <View style={styles.leftView}>
                        <Icon
                            onPress={()=>this.props.navigation.pop()}
                            name="arrowleft"
                            type="antdesign"
                            iconStyle={styles.iconStyle}
                        />
                    </View>
                    <View style={styles.middleView}>
                        <Text style={styles.textA}>Legend</Text>
                    </View>
                </View>

                <View style={styles.contentWraper}>

                 <FlatList
                 data={myLegends}
                 keyExtractor={(item)=>item.id}
                 renderItem={this.renderItem}
                 />

                </View>



            </View>
        )
    }
}
