import React, {Component} from 'react';
import {Text, View, Image, ScrollView, Pressable} from 'react-native';
import { heightPercentageToDP as hp,widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {fos , foe}from '../../Constants/images'
import {black, DANGER} from '../../Constants/colors';
import styles from './style';


let  Organization = 'friendoftheearth.org'

export default class AboutUs extends Component {
  render() {
    return (
      <View style={styles.wraper}>
        <View style={styles.header}>
          <View style={styles.left}></View>
          <View style={styles.middle}>
          <Text style={styles.textA}>About us</Text>
          </View>
          <Pressable 
           onPress={()=>this.props.navigation.navigate('Legend')}
           style={styles.right}>
          <Text style={styles.textL}>Legend</Text>
          </Pressable>
        </View>

<ScrollView>
        <View style={styles.largeText}>
          <Text style={styles.ex_largeText}>Sustainable</Text>
          <Text style={styles.ex_largeText}>Restaurants</Text>
        </View>

        <View style={styles.textView}>
          <Text style={styles.textStyle}>
            The Sustainable Restaurant project has been launched by the  
            <Text style={{...styles.textStyle,color:DANGER}}> World Sustainability Organization,
              <Text style={{color: black}}> a Non Governmental Organization (ONG).</Text>
            </Text>
          </Text>
        </View>


        <View style={styles.textView}>
          <Text style={styles.textStyle}>
            The aim of the project is to promote the conservation of natural
            resources and habitats with the involvement of both restaurateurs
            and consumers, pointing out the importance of
            <Text style={{...styles.textStyle,color:DANGER}}> Friend of the Earth </Text>
            and
            <Text style={{...styles.textStyle,color:DANGER}}> Friend of the Sea </Text>
            certified sustainable products.
          </Text>
        </View>

        <View style={styles.Logos}>

          <View style={styles.logoDes}>
            <Image source={fos} style={styles.imgLogo}/>
            <Text style={styles.logoText}>{Organization.length>15? `${Organization.slice(0,15)}...`: Organization}</Text>
          </View>

          <View style={styles.logoDes}>
            <Image source={foe} style={styles.imgLogo}/>
            <Text style={styles.logoText}>{Organization.length>15? `${Organization.slice(0,15)}...`: Organization}</Text>
          </View>
          
        </View>

     <View style={{...styles.textView,marginBottom:hp(3)}}>
        <Text style={styles.textStyle}>
          {`Certification are conducted by qualified auditors from third- party certification bodies , accredited by National Accreditation Bodies. Yearly audits are carried out on site against the strict Friend of the Sea environmental sustainability and social accountability criteria.`}
        </Text>
        </View>

        </ScrollView>
      </View>
    );
  }
}
