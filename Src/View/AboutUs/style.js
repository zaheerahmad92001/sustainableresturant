import react from 'react';
import { Platform, StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { appMarginTop } from '../../Constants/appStyle';
import { black, offwhite, TEXT_INPUT_LABEL, White } from '../../Constants/colors';
import { ex_medium, mediumText } from '../../Constants/fonts';

const styles = StyleSheet.create({
  wraper: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    marginTop:Platform.OS=='ios'? 30:0,
    alignItems: 'center',
    justifyContent: 'center',
    height: hp(8),
    backgroundColor:offwhite,
  },
  left: {
    flex: 0.1,
  },
  middle: {
    flex: 0.65,
    alignItems: 'center',
    paddingLeft:10,
  },
  right: {
    flex: 0.2,
    alignItems: 'center',
  },

  textA: {
    color: black,
    // marginLeft: wp(42.5),
    // flex:0.6,
    fontSize: mediumText,
    fontWeight: '500'
  },

  textL: {
    color: black,
    fontSize: mediumText,
    fontWeight: '500',
    // alignSelf: "flex-end",
  },

  largeText: {
    backgroundColor: 'red',
    paddingVertical: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textPara: {
    marginTop: hp(1.5),
  },
  Logos: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  logoDes: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgLogo: {
    height: 120,
    width: 150,
    resizeMode: 'contain',
  },
  logoText: {
    color: black,
    fontSize: mediumText,
  },


  ex_largeText: {
    fontSize: 40,
    color: White,
    fontWeight: 'bold',
  },
  textView: {
    marginTop: hp(3),
    marginHorizontal: 15,
  },
  textStyle: {
    fontSize: ex_medium,
    color: black,
    fontWeight: '500',
  }


});

export default styles;
