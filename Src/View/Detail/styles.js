import React from "react";
import { StyleSheet } from "react-native";
import { appMarginTop } from "../../Constants/appStyle";
import { black, grey, offwhite, White } from "../../Constants/colors";
import { mediumText, smallText } from "../../Constants/fonts";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";


const styles = StyleSheet.create({
    wraper:{
        flex:1,
        backgroundColor:'white'

    },
    header:{
        flexDirection: 'row',
        paddingTop:appMarginTop,
        alignItems:'center',
        backgroundColor:offwhite,
        paddingBottom:10,
    
      },
      leftView:{
        flex:0.2,
        alignItems:'center',
      },
      middleView:{
        flex:0.6,
        alignItems:'center',
      },
      rightView:{
        flex:0.2,
        alignItems:'center',
      },
      iconStyle:{
        color:black,
        fontSize:20,  
        },
        iconStyle2:{
            fontSize:10,
            color:grey
        },
        textA: {
          color: black,
          fontSize:smallText,
          fontWeight:'500',
          alignSelf:'center',
        },
        bottomView:{
            position:'absolute',
            bottom:hp(5),
            backgroundColor:'red',
            paddingVertical:10,
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            paddingHorizontal:60,
            borderRadius:20,
        },
        buttonStyle:{
            color:White,
            fontSize:15,
            fontWeight:'600',
        },
        nameView:{
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center',
            marginHorizontal:20,
            marginTop:20,
        },
        nameHeading:{
            fontSize:20,
            fontWeight:'400',
            color:black,
        },
        circle:{
            width:35,
            height:35,
            borderWidth:1,
            borderColor:grey,
            borderRadius:35/2,
            justifyContent:"center",
            alignItems:"center",
            alignSelf:'flex-start',
            marginTop:5,
        },
        locationStyle:{
            color:black,
            fontSize:16,
            fontWeight:'400'
        },
        rowStyle:{
            flexDirection:'row',
            marginTop:10,
            alignItems:'center',
        },
        distStyle:{
            fontSize:14,
            fontWeight:'500',
            color:grey,
            marginLeft:5,
        },
        smallImg:{
            width:30,
            height:30,
        },
        map:{
            marginTop:10,
            height:200,
            width:400,
        },
        contentView:{
            marginTop:40,
            marginHorizontal:20

        },
        textStyle:{
            color:black,
            fontSize:14,
            fontWeight:'500',
            marginLeft:15,
        },
        divider:{ 
            marginTop: 20,
            width: wp(100),
            overflow:'hidden', 
            marginLeft:20
         }
})

export default styles