import React, { Component } from "react";
import { View, Text, Linking, Image, ScrollView , Platform, Pressable } from 'react-native'
import { Divider, Icon } from "react-native-elements";
import Swiper from 'react-native-swiper'
import { black, offwhite, White } from "../../Constants/colors";
import { nature, foe, fos } from "../../Constants/images";
import MyMaps from '../../Components/MyMaps';


import styles from "./styles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const imgHeight = 180

export default class Detail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            region: {
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121
            }
        }
    }

    dotStyle = () => {
        return (
            <View style={{
                // backgroundColor:'',
                width: 8,
                height: 8,
                borderRadius: 4,
                borderWidth: 1,
                borderColor: White,
                marginLeft: 3, marginRight: 3, marginTop: 3,
                marginBottom: 3,
            }} />
        )
    }

    activeDotStyle = () => {
        return (
            <View style={{
                backgroundColor: offwhite,
                width: 8, height: 8,
                borderRadius: 4,
                borderColor: offwhite,
                borderWidth: 1,
                marginLeft: 3,
                marginRight: 3,
                marginTop: 3,
                marginBottom: 3,
            }}
            />
        )
    }
    SwiperView() {
        return (
            <View style={{ height: imgHeight, marginTop: 10, backgroundColor: 'red' }}>
                <Swiper
                    loop={true}
                    autoplay={true}
                    autoplayTimeout={5}
                    autoplayDirection={true}
                    dot={this.dotStyle()}
                    activeDot={this.activeDotStyle()}
                    style={{ height: (imgHeight), }}
                    containerStyle={{ height: imgHeight }}
                >
                    {[...Array(3).keys()].map((item, index) => {
                        return (
                            <View style={{ height: imgHeight }}>
                                <Image
                                    source={nature}
                                    style={{ height: imgHeight, width: '100%', }}
                                />
                            </View>
                        )
                    })}
                </Swiper>
            </View>
        )
    }


     openMapDirections=()=>{
        const {region}= this.state
        let latitude = 31.4670
        let longitude = 74.2519
     const url = Platform.select({
         ios:`googleMaps://app?saddr=${region.latitude},${region.longitude}&daddr=${latitude},${longitude}`,
         android: `geo://?q=${latitude},${longitude}`,
       });
    //    console.log('url' ,url)
 
       Linking.canOpenURL(url)
         .then((supported) => {
           if (supported) {
            //    console.log('supported' , supported)
             return Linking.openURL(url);
           } else {
             const browser_url = `https://www.google.de/maps/@${latitude},${longitude}`;
             return Linking.openURL(browser_url);
           }
         })
         .catch(() => {
           if (Platform.OS === 'ios') {
             Linking.openURL(
               `maps://?q=${latitude},${longitude}`,
             );
           }
         });
     }

     openAdvisorLink=()=>{
        const link = 'https://www.tripadvisor.com/'
        Linking.canOpenURL(link).then((supported)=>{
            if(supported){
                return Linking.openURL(link)
            }
        })

     }

     openWebLink=()=>{
        const link = 'http://www.mondomarevivo.com/'
        Linking.canOpenURL(link).then((supported)=>{
            if(supported){
                return Linking.openURL(link)
            }
        })
     }

      makeCall() {
        let phone =`+39 0836 9430007`
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${phone}`;
        }
        else {
            phoneNumber = `tel:${phone}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    Alert.alert('Phone number is not available');
                } else {
                    console.log('yes can open')
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));
    }

     


    render() {
        return (
            <View style={styles.wraper}>
                <View style={styles.header}>
                    <View style={styles.leftView}>
                        <Icon
                            onPress={() => this.props.navigation.pop()}
                            name="arrowleft"
                            type="antdesign"
                            iconStyle={styles.iconStyle}
                        />
                    </View>
                    <View style={styles.middleView}>
                        <Text style={styles.textA}>Ristorante Hotel Panoramico</Text>
                    </View>
                </View>
                <ScrollView>
                    <View>
                        {this.SwiperView()}
                        <View style={styles.nameView}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.nameHeading}>Ristorante Hotel Panoramico</Text>
                                <Text style={styles.locationStyle}>Via Panoramica , 100 , 73030 Castro (LE). Italy</Text>
                                <View style={styles.rowStyle}>
                                    <Icon
                                        name="location-arrow"
                                        type="font-awesome-5"
                                        iconStyle={styles.iconStyle2}
                                    />
                                    <Text style={styles.distStyle}>5490.65 km</Text>
                                </View>

                                <View style={styles.rowStyle}>
                                    <Image source={fos} style={styles.smallImg} />
                                    <Text style={{ ...styles.distStyle, color: black }}>1</Text>
                                </View>
                            </View>
                            <View style={styles.circle}>
                                <Icon name='star' type='evilicon' />
                            </View>

                        </View>
                        <View style={styles.map}>
                            <MyMaps
                                customStyle={styles.map}
                            />
                        </View>

                        <View style={styles.contentView}>
                            <Pressable 
                            onPress={()=>this.makeCall()}
                             style={styles.rowStyle}>
                                <View style={{alignSelf:'flex-start'}}>
                                <Icon
                                    name="phone"
                                    type="feather"
                                    iconStyle={[styles.iconStyle,]}
                                />
                                </View>
                                <View >
                                    <Text style={styles.textStyle}>033278786543</Text>
                                    <Divider style={styles.divider}/>

                                </View>

                            </Pressable>


                            <Pressable 
                            onPress={()=>this.openWebLink()}
                              style={styles.rowStyle}>
                                <View style={{alignSelf:'flex-start'}}>
                                <Icon
                                    name="link"
                                    type="feather"
                                    iconStyle={styles.iconStyle}
                                />
                                </View>
                                <View >
                                    <Text style={styles.textStyle}>www.mondomarevivo.co</Text>
                                    <Divider style={styles.divider}/>
                                </View>

                            </Pressable>


                            <Pressable 
                            onPress={()=>this.openAdvisorLink()}
                              style={{...styles.rowStyle,marginBottom:hp(15),}}>
                                <View style={{alignSelf:'flex-start'}}>
                                <Icon
                                    name="arrow-up-right"
                                    type="feather"
                                    iconStyle={styles.iconStyle}
                                />
                                </View>
                                <View >
                                    <Text style={styles.textStyle}>https://tripadvisor.it</Text>
                                    <Divider style={styles.divider}/>
                                    
                                </View>

                            </Pressable>


                        </View>
                    </View>
                </ScrollView>

                <Pressable
                onPress={()=>this.openMapDirections()} 
                 style={styles.bottomView}>
                    <Text style={styles.buttonStyle}>Get directions</Text>
                </Pressable>

            </View>
        )
    }
}