import React from "react";
import { View, Text, StyleSheet, Image } from 'react-native'
import { black } from "../Constants/colors";
import { group } from "../Constants/images";

const NotificationList = (props) => {
    return (
        <View style={styles.wraper}>
            <View style={styles.imgView}>
                <Image
                    source={group}
                    style={styles.imgstyle}
                />
            </View>
            <View style={styles.middleText}> 
              <Text style={styles.textStyle}>
                {'iZito uses functional cookies and non-personalized content. Click ok to allow us and our partners to use your data for the best experience'}
              </Text>
              <Text style={styles.dateStyle}>jun 20, 2018</Text>

            </View>
            <View style={styles.rightView}></View>
        </View>
    )
}

export default NotificationList

const styles = StyleSheet.create({
    wraper: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom:20,

    },
    imgView: {
        alignSelf: 'flex-start',
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        overflow: 'hidden',

    },
    middleText: {
     flex:1,
     marginLeft:5,
    },
    rightView: {
    },
    imgstyle: {
        width: undefined,
        height: undefined,
        flex: 1,
    },
    contentWrape:{
        marginHorizontal:20,
        marginTop:20,
    },
    textStyle:{
        fontSize:14,
        color:black,
        fontWeight:"400",
    },
    dateStyle:{
        textAlign:'right',
         marginTop:5,
         fontSize:12,
         fontWeight:'400',
        }

})