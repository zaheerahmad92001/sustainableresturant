import React from "react";
import {View , Text , StyleSheet} from 'react-native'
import { Icon } from "react-native-elements";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import RNGooglePlaces from 'react-native-google-places';

const GooglePlaces =(props)=>{
    return(
        <GooglePlacesAutocomplete
        placeholder='Search'
        onPress={(data, details = null) => {
          console.log(details);
        }}
        query={{
          key: 'AIzaSyDt9GY0qjMwSFvi-ODbrRJFZg3wCwtZofc',
          language: 'en',
          types: "route",
        }}
        GooglePlacesDetailsQuery={{
            fields: 'formatted_address',
          }}
          debounce={1000}
      />
    );
}
export default GooglePlaces

const styles = StyleSheet.create({
    wraper:{
        flex:1
    }
})