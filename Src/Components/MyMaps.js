import React from "react";
import {View , Text ,StyleSheet} from 'react-native'
import { Icon } from "react-native-elements";
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

const MyMaps=(props)=>{
    return(
        <View style={[styles.container,props.customStyle]}>
        <MapView
          // provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
        >
        </MapView>
      </View>
    )
}
export default MyMaps;

const styles = StyleSheet.create({
    wraper:{
        // flex:1,
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        // height:'100%',
        // width:'100%',
        height:400,
        width:400,
        justifyContent: 'flex-end',
        alignItems: 'center',
      },
      map: {
        ...StyleSheet.absoluteFillObject,
      },
})