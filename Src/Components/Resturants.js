import React from 'react'
import {View,Text , StyleSheet ,Image, TouchableOpacity, Pressable } from 'react-native'
import { Icon } from 'react-native-elements'
import { heightPercentageToDP as hp } from 'react-native-responsive-screen'

import {foe} from '../Constants/images'
import { black, grey, lightGrey, White, yellow } from '../Constants/colors'
import { largeText, mediumText, smallText } from '../Constants/fonts'

const Restaurant=(props)=>{
    const {onPress} =props
    return(
     <View style={styles.wraper}>
         <Pressable
         onPress={onPress} 
         style={styles.content}>
        <View>

           <View style={styles.row}>
               <View style={{alignSelf:'flex-start'}}>
                <Icon
                 name='crown'
                 type='font-awesome-5'
                 iconStyle={{fontSize:15,color:yellow}}
                />
               </View>
               <Text style={[styles.bigText,{top:-2}]}>Temakinho Turin Ri</Text>
           </View>

           <Text style={styles.standeredText}> Via Guesesppe Luigi Lanugage 15,1010101, Italy</Text>

           <View style={{...styles.row,marginTop:5}}>
             <Image  source={foe} style={styles.smallImg}/> 
             <Text style={styles.count}>5</Text>
           </View>

        </View>

         </Pressable>

         <View style={styles.imgContetn}>
           <View style={styles.imgView}>    
             <Image source={foe} style={styles.imgStyle}/>
          </View>
          <TouchableOpacity 
           style={styles.circle}>
            <Icon name='star' type='evilicon'/>
          </TouchableOpacity>
         </View>

     </View>
    )
}

export default Restaurant

const styles = StyleSheet.create({
    wraper:{
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:White,
        paddingVertical:hp(2),
        paddingHorizontal:10,
        // marginBottom:10,
        borderBottomColor:lightGrey,
        borderBottomWidth:1,

    },
    content:{
        flex:1,
        alignSelf:'flex-start',
    },
    imgContetn:{
        
    },
    imgView:{
     width:110,
     height:110,
     overflow:"hidden",
     backgroundColor:lightGrey,
    },
    imgStyle:{
      width:undefined,
      height:undefined,
      flex:1,
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
    },
    bigText:{
        fontSize:largeText,
        fontWeight:'500',
        marginLeft:5,
    },
    standeredText:{
        color:grey,
        fontSize:smallText,
        fontWeight:'500',
        marginTop:5,
    },
    smallImg:{
        width:30,
        height:30,

    },
    count:{
        fontSize:smallText,
        fontWeight:'500',
        color:black,
        marginLeft:5,
    },
    circle:{
        backgroundColor:White,
        width:25,
        height:25,
        borderRadius:25/2,
        position:'absolute',
        right:10,
        top:10,
        justifyContent:'center'
    }
})

