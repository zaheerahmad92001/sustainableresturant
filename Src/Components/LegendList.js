import React from "react";
import { View, Text, StyleSheet, Image } from 'react-native'
import { Icon } from "react-native-elements";
import { black, White, yellow } from "../Constants/colors";
import { group } from "../Constants/images";

const LegendList = (props) => {
    const {item} = props
    return (
        <View style={styles.wraper}>
        
            <View style={styles.imgView}>
                {item?.img?
                <Image
                    source={item?.img}
                    style={styles.imgstyle}
                />:
                <View style={styles.goldView}>
                <Icon
                name='crown'
                type='font-awesome-5'
                iconStyle={{ fontSize: 15, color:White }}
              />
              </View>
}
            </View>

            
        <View style={[styles.middleText,{marginLeft:10,}]}>
            <Text style={styles.nameStyle}>{item?.name}</Text>
            {/* <View style={styles.middleText}>  */}
              <Text style={styles.textStyle}>{item?.description}</Text>
            {/* </View> */}
        </View>
            <View style={styles.rightView}></View>
        </View>
    )
}

export default LegendList

const styles = StyleSheet.create({
    wraper: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom:20,

    },
    imgView: {
        alignSelf: 'flex-start',
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        overflow: 'hidden',

    },
    middleText: {
     flex:1,
    //  marginLeft:5,
    },
    rightView: {
    },
    imgstyle: {
        width: undefined,
        height: undefined,
        flex: 1,
    },
    contentWrape:{
        marginHorizontal:20,
        marginTop:20,
    },
    textStyle:{
        fontSize:14,
        color:black,
        fontWeight:"400",
    },
    dateStyle:{
        textAlign:'right',
         marginTop:5,
         fontSize:12,
         fontWeight:'400',
        },
        row:{
            flexDirection:'row',
            alignItems:'center',
            justifyContent:'center'

        },
        nameStyle:{
            fontSize:17,
            color:black,
            fontWeight:'500',
            marginBottom:2

        },
        goldView:{
            backgroundColor:yellow,
            width:30,
            height:30,
            borderRadius:30/2,
            justifyContent:'center',
            alignItems:'center'
        }

})