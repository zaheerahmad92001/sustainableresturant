import { createStore } from 'redux';
// import { persistStore, persistReducer,createTransform } from 'redux-persist';
// import thunk from 'redux-thunk';
import reducers from '../reducers';

// const persistConfig = {
//     key: 'root',
//     storage:AsyncStorage,
//     whitelist: ['User'],
//     setTimeout:null
   
//   }
 
// const persistedReducer = persistReducer(persistConfig, reducers)	
// const store = createStore(persistedReducer,applyMiddleware(thunk));
// // let persistor = persistStore(store)
let store = createStore(reducers)

export {store};
