
import types from './types'

export const ChangeLanguage = (payload) => {
    return {
        type: types.CHANGE_LANGUAGE,
        payload: payload
    }
}
