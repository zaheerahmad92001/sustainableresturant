//Colors

export const TEXT_INPUT_LABEL = '#7a7a77';
export const black = 'black';
export const DANGER = '#ED0707';
export const White = 'white';
export const grey = '#969592';
export const yellow ='#FDDA0D';
export const lightGrey = '#D7D7D7';
export const offwhite = '#FAF9F6';