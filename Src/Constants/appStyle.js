import { getStatusBarHeight, isIphoneX } from "react-native-iphone-x-helper";
import{ Platform} from 'react-native'


export const appMarginTop = Platform.OS=='android'? getStatusBarHeight()-10: isIphoneX() ?getStatusBarHeight()+20:getStatusBarHeight()+10