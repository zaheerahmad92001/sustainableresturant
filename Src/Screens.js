import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { heightPercentageToDP as hp,widthPercentageToDP as wp} from 'react-native-responsive-screen';

//Screens
import SplashScreen from './View/Splash';
import Home from './View/Home';
import AboutUs from './View/AboutUs';
import Setting from './View/Setting';
import Favourites from './View/Favourites';
import Language from './View/Language';
import MetricSystem from './View/MetricSystem';
import Notifications from './View/Notifications';
import Legend from './View/Legend';
import Detail from './View/Detail';

import {DANGER, grey} from './Constants/colors';
import {home , favourites , group , setting} from './Constants/images'

import { connect } from 'react-redux';
import I18n from './i18n';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

function BottomTab() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarHideOnKeyboard: true,
        tabBarShowLabel: false,
        tabBarStyle: {
          elevation: 0,
          backgroundColor: '#ffffff',
          height: 65,
          ...styles.shadow,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({focused}) => (
            <View
              style={{
                marginTop: hp(2),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={home}
                resizeMode="contain"
                style={{
                  width: 25,
                  height: 25,
                  tintColor: focused ? DANGER : grey,
                }}
              />
              <Text
                style={{
                  color: focused ? DANGER : grey,
                  ...styles.text,
                }}>
                Restaurants
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="AboutUs"
        component={AboutUs}
        options={{
          tabBarIcon: ({focused}) => (
            <View
              style={{
                marginTop: hp(2),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={group}
                resizeMode="contain"
                style={{
                  width: 25,
                  height: 25,
                  tintColor: focused ? DANGER : grey,
                }}
              />
              <Text
                style={{
                  color: focused ? DANGER : grey,
                  ...styles.text,
                }}>
                About us
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Favourites"
        component={Favourites}
        options={{
          tabBarIcon: ({focused}) => (
            <View
              style={{
                marginTop: hp(2),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={favourites}
                resizeMode="contain"
                style={{
                  width: 25,
                  height: 25,
                  tintColor: focused ? DANGER : grey,
                }}
              />
              <Text
                style={{
                  color: focused ? DANGER : grey,
                  ...styles.text,
                }}>
                Favourite
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Setting"
        component={Setting}
        options={{
          tabBarIcon: ({focused}) => (
            <View
              style={{
                marginTop: hp(2),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={setting}
                resizeMode="contain"
                style={{
                  width: 25,
                  height: 25,
                  tintColor: focused ? DANGER : grey,
                }}
              />
              <Text
                style={{
                  color: focused ? DANGER : grey,
                  ...styles.text,
                }}>
                Setting
              </Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
}

function SplashNavigator(){
  return(
    <Stack.Navigator 
    initialRouteName="SplashScreen"
    screenOptions={{headerShown: false}}>
    <Stack.Screen  name='Splash' component={SplashScreen}/>
    </Stack.Navigator>
  )
}


const AppContainer=(props)=>{
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="SplashScreen"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="BottomTab" component={BottomTab} />
        <Stack.Screen name='SplashScreen' component={SplashNavigator}/>
        <Stack.Screen name = 'Notifications' component={Notifications}/>
        <Stack.Screen name='Language' component={Language}/>
        <Stack.Screen name='Legend' component={Legend}/>
        <Stack.Screen name='Detail' component={Detail}/>
        <Stack.Screen name='MetricSystem' component={MetricSystem}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => ({
  language: state.LanguageReducer.language
})

export default connect(mapStateToProps)(AppContainer)

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#7F5DF0',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5,
  },
  view: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 10,
  },
  text: {fontSize: 12, paddingBottom: 15},
  image: {
    width: 25,
    height: 25,
  },
});
