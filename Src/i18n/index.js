import I18n from 'i18n-js';

import en from '../i18n/en.json'
import it from '../i18n/it.json'

I18n.translations = {
    //define all imports
    en,
    it,
}

I18n.fallbacks = true;
export default I18n;
